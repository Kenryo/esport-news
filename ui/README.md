## Projet Twitter

Authors : Adrien Le Deunff, Paul Bernasconi

Le repo se compose d'une partie front et d'une partie back.

## Pour lancer le projet :

- git clone <url_du_git>

/!\ Attention : étant donné que l'application fonctionne grâce à l'api de tweeter qui doit être obtenue au préalable via un compte Twitter développeur, il ne faut pas oublier de placer votre clé à la racine du dossier back dans un fichier ".env" que vous venez de créer, ce fichier contiendra la clé sous ce format :

TWITTER_CONSUMER_KEY= xxxxx <br />
TWITTER_CONSUMER_SECRET= xxxxxx <br />
TWITTER_ACCESS_TOKEN_KEY= xxxxxx <br />
TWITTER_ACCESS_TOKEN_SECRET= xxxxxx <br />

- docker-compose up --build <br />
si votre image n'est pas encore construite

- docker-compose up -d <br />
si vous avez déjà construit l'image

Le docker-compose.yml va executer le Dockerfile qui va s'occuper de faire un npm install pour installer les dépendances, exposer le port 3000 et effectuer un npm start

## Sur votre navigateur :

localhost:3000

Et voila, vous êtes sur l'application, vous pouvez dès maintenant rechercher des Tweets en fonction de vos centres d'interêts.

Le projet devait pouvoir afficher toutes les actualitées et réactions concernant les évènements e-sport en France.