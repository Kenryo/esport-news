import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';


import logo from './logo.svg';
import axios from 'axios';

import './App.css';

const apiUrl = `http://localhost:8080`;

export default class App extends Component {
  state = {
    searchQuery: '',
    tweets: []
  };

  constructor(props) {
    super(props);
    this.searchTweets = this.searchTweets.bind(this);
  }

  async searchTweets() {
    console.log(this.state.searchQuery)
    const res = await axios.get(apiUrl + '/tweets?q=' + this.state.searchQuery + '&lang=fr');
    console.log(apiUrl + '/tweets?q=' + this.state.searchQuery + '&lang=fr');
    console.log(res);
    this.setState({
      tweets: res.data.statuses
    });
    console.log(this.state.tweets)
  }

  handleChange = (event) => {
    this.setState({
      searchQuery: event.target.value
    });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <input type="text" value={this.state.searchQuery} onChange={this.handleChange}/>
          <Button variant="contained" color="primary" onClick={this.searchTweets}>
            Rechercher
          </Button>
          <p>Liste de tweets :</p>
          <ul>
            {this.state.tweets.map(tweet => (
              <li key={tweet.id}>{tweet.user.name} : {tweet.text}</li>
            ))}
          </ul>
        </header>
      </div>
    );
  }

}
