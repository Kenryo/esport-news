const dotenv = require('dotenv');
dotenv.config();
module.exports = {
  my_consumer_key: process.env.TWITTER_CONSUMER_KEY,
  my_consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
  my_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
  my_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
};