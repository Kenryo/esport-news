var Twitter = require('twitter');
const { my_consumer_key, my_consumer_secret, my_token_key, my_token_secret} = require('./config');

// Connexion à l'api Twitter
var client = new Twitter({
    consumer_key: my_consumer_key,
    consumer_secret: my_consumer_secret,
    access_token_key: my_token_key,
    access_token_secret: my_token_secret
  });


module.exports = client;