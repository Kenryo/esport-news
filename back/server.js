const express = require("express");
const app = express();
const connectDb = require("./src/connexion");
const User = require("./src/User.model");
const client = require("./src/twitter");
const cors = require('cors');

const PORT = 8080;

app.use(cors());



app.get("/tweets", (req, res) => {  
  var params = {};
  Object.keys(req.query).forEach( (key) => {
    params[key] = req.query[key];
  })
  client.get('https://api.twitter.com/1.1/search/tweets.json', params, function(error, tweets, response) {
    res.json(tweets);
  });
  
});

app.listen(PORT, function() {
  console.log(`Listening on ${PORT}`);

  connectDb().then(() => {
    console.log("MongoDb connected");
  });
});


